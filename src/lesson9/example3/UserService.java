package lesson9.example3;

import java.util.InputMismatchException;
import java.util.Scanner;

public class UserService {
    public static void main(String[] args) {

        User user = new User();
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите имя");

        String inputName = scan.next();
        System.out.println("Считал " + inputName);

//        UserHandler userHandler = new UserHandler();
//        try {
//           userHandler.callUser(user, inputName);
//        } catch (Throwable e) {
//            System.out.println("Словил ошибку " + e.fillInStackTrace());
//        }//
//        System.out.println("Отработало до конца ");

        try {
        user.name(inputName);}
        catch (InputMismatchException e){
            System.out.println("Введите корректное значение имени или программа будет завершена");
            user.name(scan.next());
        }

    }

}
