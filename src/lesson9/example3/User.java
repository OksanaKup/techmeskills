package lesson9.example3;

import java.util.InputMismatchException;

public class User {

    String name;

    public String name(String name) throws InputMismatchException {
//        Integer numbers = Integer.parseInt(name);
//        if (numbers == null)
//            System.out.println("Имя корректно");

        if (name.contains("1") || name.contains ("2") || name.contains ("3") || name.contains ("4") || name.contains ("5")){
            System.out.println("Данные введены неверно");
            throw new InputMismatchException();
        }

        this.name = name;  //свойству присвоить значение входящего параметра
        return this.name;
    }
}
