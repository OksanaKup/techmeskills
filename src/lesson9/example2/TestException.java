package lesson9.example2;

import java.util.InputMismatchException;
import java.util.Scanner;

public class TestException {

    public static void main(String[] args) {

        System.out.println("Введите что-нибудь");

        try {
            int[] numbers = new int[5];
            Scanner scan = new Scanner(System.in);
//            System.out.println(scan.nextInt());
            for (int i = 0; i <= numbers.length; i++) {
                numbers[i] = scan.nextInt();
                System.out.println("Полученные значения:" + " " + numbers[i]);
            }

        } catch (ArrayIndexOutOfBoundsException x) {
            System.out.println("Проблема с индексом");

        } catch (InputMismatchException t) {
            System.out.println("Вы ввели фигню");

        } finally {
            System.out.println("Расчет окончен");
        }


    }
}
