package lesson9.example4HW;

public class Circle extends ShapeScanAlt {

    double radius;
    double pi = 3.14;
    Circle (double radius){
        this.radius = radius;
    }
    @Override
    public double getArea() {
        return radius * radius * pi;
    }
    public double getPerimeter(){
        return 2 * pi * radius;
    }
}
