package lesson9.example4HW;

public class Rectangle extends ShapeScanAlt{
    double weight;
    double height;
    Rectangle (double weight, double height){
        this.weight = weight;
        this.height = height;
    }

    @Override
    public double getArea() {
        return weight * height;
    }
    public double getPerimeter(){
        return 2 * ( weight + height );
    }
}
