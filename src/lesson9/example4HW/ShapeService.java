package lesson9.example4HW;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ShapeService {

    public static void main(String[] args) {

        System.out.println("Введите радиус круга; затем длину и ширину прямоугольника ");

        Circle circle;
        Rectangle rectangle;

        try {
            Scanner firstScan = new Scanner(System.in);
            //Если объект circle объявить здесь а не выше, то в блоке catch circle и rectangle будут не доступны
            double radiusInput = firstScan.nextDouble();
            double weightInput = firstScan.nextDouble();
            double heightInput = firstScan.nextDouble();

            circle = new Circle(radiusInput);
            rectangle = new Rectangle(weightInput, heightInput);

        } catch (InputMismatchException v) {
            Scanner repeatScan = new Scanner(System.in);
            System.out.println("Введите корректные значения радиус круга; затем длину и ширину прямоугольника " +
                    "или программа будет завершена c ошибкой");
            double radiusInput = repeatScan.nextDouble();
            double weightInput = repeatScan.nextDouble();
            double heightInput = repeatScan.nextDouble();
            circle = new Circle(radiusInput);
            rectangle = new Rectangle(weightInput, heightInput);
        }


        System.out.println("Площадь круга равна: " + circle.getArea());
        System.out.println("Периметр окружности равен: " + circle.getPerimeter());
        System.out.println("Площадь прямоугольника равна: " + rectangle.getArea());
        System.out.println("Периметр прямоугольника равен: " + rectangle.getPerimeter());
    }
}





