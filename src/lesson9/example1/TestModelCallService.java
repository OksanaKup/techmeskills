package lesson9.example1;

import lesson9.example1.TestModel;

public class TestModelCallService {

    public static void main(String[] args) {

        String name = "Test";
        String pre = "Тестовый префикс для Сотрудника";


        TestModel testModel = new TestModel();

        //Вывести фиксированное имя, которое нельзя менять
        //System.out.println(testModel.getConstName());

        //Изменять значения name в объекте тест модел
        //System.out.println(testModel.setName(name));

        //Выводить имя с префиксом, значение префикса : Сотрудник Василий, Менеджер Василий
        testModel.setName(name);
        testModel.displayInfo(pre);



        TestModel testModel2 = new TestModel();
        //testModel2.name = "Peti";
        testModel2.setName("Kolya");
        System.out.println(testModel2.setName(testModel2.name));

        //срть два обта ссылочного типа  - делаем через equals
        if (testModel.equals(testModel2)){
            System.out.println("Они равны");
        } else {
            System.out.println("Они НЕ равны");
        }

    }

}
