package lesson9.example5HW;

import java.util.InputMismatchException;
import java.util.Scanner;

public class UserServiceNew {

    public static void main(String[] args) {

        UserNew user = new UserNew();
        Scanner scan = new Scanner(System.in);
        Scanner scan2 = new Scanner(System.in);

        System.out.println("Введите имя и фамилию, разделив их с помощью Enter ");

        String inputName = scan.next();
        String inputSurname = scan2.next();

        //System.out.println("Считал " + inputName + " " + inputSurname);

        try {
            user.name(inputName);
        } catch (InputMismatchException e) {
            System.out.println("Введите корректное значение имени или программа будет завершена");
            user.name(scan.next());
        }

        try {
            user.surname(inputSurname);
        } catch (InputMismatchException d) {
            System.out.println("Введите корректное значение фамилии или программа будет завершена");
            user.surname(scan2.next());
        }

        System.out.println("Успех");
    }
}
