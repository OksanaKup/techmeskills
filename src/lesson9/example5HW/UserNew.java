package lesson9.example5HW;

import java.util.InputMismatchException;

public class UserNew {

    String name;
    String surname;

    public String name(String name) throws InputMismatchException {

        if (name.contains("1") || name.contains("2") || name.contains("3") || name.contains("4") || name.contains("5")) {
            System.out.println("Данные введены неверно");
            throw new InputMismatchException();
        }
        this.name = name;  //свойству присвоить значение входящего параметра
        return this.name;
    }

    public String surname(String surname) throws InputMismatchException {
        if (surname.contains("1") || surname.contains ("2") || surname.contains ("3") || surname.contains ("4") || surname.contains ("5")){
            System.out.println("Данные введены неверно");
            throw new InputMismatchException();
        }
        this.surname = surname;  //свойству присвоить значение входящего параметра
        return this.surname;
    }

}



