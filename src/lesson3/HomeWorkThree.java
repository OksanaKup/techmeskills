package lesson3;

public class HomeWorkThree {

    public static void main(String[] args) {

        sortMax();
        sortMin();
        getMaxValue();
    }

    private static void sortMax() {
        int[] arr = new int[]{10, 12, 5, 4, 8, 3};
        System.out.println("Сортировка массива по возрастанию: ");
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    int tmp = arr[j + 1];
                    arr[j + 1] = arr[j];
                    arr[j] = tmp;
                }
            }
        }
        for (int a : arr) {
            System.out.print(a + " ");
        }
    }

    private static void sortMin() {

        int[] arr = new int[]{3, 2, 1, 4, 10, 7};
        System.out.println();
        System.out.println("Сортировка массива по убыванию: ");
        for (int i = 0; i < arr.length-1; i++) {
            //for (int j = 0; j < arr.length - 1; j++) {
            if (arr[i] < arr[i + 1]) {
                int tmp = arr[i + 1];
                arr[i + 1] = arr[i];
                arr[i] = tmp;
                i = -1;
            }
            //}
        }
        for (int a : arr) {
            System.out.print(a + " ");
        }
    }

    private static void getMaxValue() {

        int[][] arrQwe = new int[][]{{56, 7, 8}, {15, 20, 510}};

        int minValue = arrQwe[0][0];

        for (int i = 0; i < arrQwe.length; i++) {
            for (int j = 0; j < arrQwe[i].length; j++) {
                if (arrQwe[i][j] < minValue)
                    minValue = arrQwe[i][j];
            }
        }
        System.out.println();
        System.out.println("Минимальное значение двумерного массива: ");
        System.out.println(minValue);
    }

}












