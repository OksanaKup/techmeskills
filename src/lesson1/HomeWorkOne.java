package lesson1;

public class HomeWorkOne {

    public static void main (String[] args) {
        leastTwo(3, 5);
        sumTwo(3, 8);
        multiplyThree(8, 6.85, 7.39 );
        residualOfDivisionTwo(3, 8 );
        changeTwoWithParams(6.85, 7.99);
    }

    private static void leastTwo(int num1, int num2) {
        if (num1 < num2) {
            System.out.println("Задание 1. Наименьшее значение равно " + num1);
        } else {
            System.out.println("Условие не выполнено.");
        }
    }

    private static void sumTwo(int num3, int num4) {
        int resultTwo = num3 + num4;
        System.out.println("Задание 2. Сумма двух значений равна " + resultTwo);
    }

    private static void multiplyThree(int num5, double num6, double num7) {
        double resultThree = num5 * num6 * num7;
        System.out.println("Задание 3. Произведение трех значений равно " + resultThree);
    }

    private static void residualOfDivisionTwo(int num3, int num5) {
        int resultFour = num5 % num3;
        System.out.println("Задание 4. Остаток от деления двух значений равен " + resultFour);
        System.out.println("Задание 5. Расчеты распределены по отдельным методам внутри одного класса. ");
    }

    private static void changeTwoWithParams(double num6, double num7) {
        int resultSix = (int) (num6 + num7);
        System.out.println("Задание 6. Результат преобразования суммы двух чисел к целочисленному значению равен " + resultSix);
    }

}

