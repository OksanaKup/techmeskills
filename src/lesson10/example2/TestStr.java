package lesson10.example2;

public class TestStr {

    public static void main(String[] args) {

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("word");

        stringBuilder.insert(3, "l");
        System.out.println(stringBuilder);


    }

    private void example4ForStr() {
        StringBuilder smallString = new StringBuilder();
        smallString.append("Рабочее название");

        for (int i = 0; i < 10; i++) {
            smallString.append(i);
        }
        System.out.println(smallString);

    }


//        stringBuilder.append("Work");
//        stringBuilder.append(" value ");
//        stringBuilder.append(" 4 ");
//
//        System.out.println(stringBuilder);


    private void example3ForStr() {
        String text = "Объявление Владелец Имя Иванов Цена 7000";
        String text2 = "Объявление Владелец Имя Петров Цена 23000";
        int numberLastSpace = 31;
        System.out.println(text.substring(0, 10));

    }

    private void ex2ForStr() {
        String full = "КреПолная";
        String notFull = "ол";
        System.out.println(full.lastIndexOf(notFull));
    }

    private void exForStr() {
        String smallString = "Рабочее название";     //так не надо склеивать строки
        for (int i = 0; i < 10; i++) {
            smallString = smallString + i;
        }
        int num = 2335;
        String numString = String.valueOf(num);
        System.out.println(smallString.charAt(2));
        System.out.println(numString);
        //System.out.println(smallString.toUpperCase());

    }

}
