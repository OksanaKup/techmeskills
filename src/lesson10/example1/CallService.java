package lesson10.example1;

public class CallService {

    public static void main(String[] args) {
        Model model = new Model();

        String test = model.getString();
        model.setNumber(1);

        int number = model.getNumber();
        System.out.println(number);

        Model model2 = new Model(2);
        System.out.println(model2.getNumber());

    }
}