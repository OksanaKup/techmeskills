package chernovick;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Chernovick {

    public static void main(String[] args) {

        try {                                                                   //ловим исключения

            int[] numbers = new int[5];
            Scanner scan = new Scanner(System.in);

            //System.out.println(scan.nextInt());
//        int a = scan.nextInt();
//        System.out.println("Вы ввели " + a);

            for (int i = 0; i <= numbers.length; i++) {
                numbers[i] = scan.nextInt();                                    //вводим последовательно элементы массива
                System.out.println("Полученные значения " + numbers[i]);
            }
        } catch (ArrayIndexOutOfBoundsException x) {
            System.out.println("Проблема с индексом массива ");

        } catch (InputMismatchException t) {
            System.out.println("Вы ввели не то ");      //выводим когда что-то идет с ошибкой
        } finally {
            System.out.println("Обработка завершена");
        }

    }

}
