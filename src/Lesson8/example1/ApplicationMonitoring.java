package Lesson8.example1;

public class ApplicationMonitoring {

    private static int countError = 3;   //зачем?

    public static void main(String[] args) {

//        MonitoringSystem genMon = new GeneralIndicatorMonitoringModule();
//        genMon.startMonitoring();
//        ErrorMonitoringModule errMon = new ErrorMonitoringModule();
//        errMon.startMonitoring();

        MonitoringSystem genMod = new MonitoringSystem() {   //создается безымянный java-класс, реализующий интерфейс MonitoringSystem
                                                             //теперь нужно реализовать все методы интерфейса MonitoringSystem
                                                             //для этого создаем объект этого как будто безымянного класса
            @Override
            public void startMonitoring() {
                System.out.println("Мониторинг отслеживания ошибок стартовал");
            }
        };

        MonitoringSystem errMod = new MonitoringSystem() {
            @Override
            public void startMonitoring() {
                System.out.println("Мониторинг отслеживания ошибок стартовал");
            }
        };

        MonitoringSystem secMod = new MonitoringSystem() {
            @Override
            public void startMonitoring() {
                System.out.println("Мониторинг отслеживания ошибок стартовал");
            }
        };

        genMod.startMonitoring();
        errMod.startMonitoring();
        secMod.startMonitoring();

    }

}




//            public int getprintCountError() {
//            return countError;   //зачем?
//        }














