package Lesson8.example4HW;

import java.util.Scanner;

public class MonthsApplication {

    public static void main(String[] args) {

        System.out.println("Введите номер месяца ");

        Scanner scan = new Scanner(System.in);
        int inputP = scan.nextInt();

        System.out.println("Вы ввели номер, соответствующий месяцу: " + Months.getNameMonth(inputP));
    }

}
