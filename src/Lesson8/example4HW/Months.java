package Lesson8.example4HW;

public enum Months {

    JANUARY(1, "Январь"),
    FEBRUARY(2, "Февраль"),
    MARCH(3, "Март"),
    APRIL(4, "Апрель"),
    MAY(5, "Май"),
    JUNE(6, "Июнь"),
    JULY(7, "Июль"),
    AUGUST(8, "Август"),
    SEPTEMBER(9, "Сентябрь"),
    OCTOBER(10, "Октябрь"),
    NOVEMBER(11, "Ноябрь"),
    DECEMBER(12, "Декабрь");

    int numberMonth;
    String nameMonth;

    Months (int numberMonth, String nameMonth){
        this.numberMonth = numberMonth;
        this.nameMonth = nameMonth;
    }

    public static String getNameMonth (int inputParamFromScanner){
        Months[] months = Months.values();
        for (Months month: months){
            if (month.numberMonth == inputParamFromScanner){
                return month.nameMonth;
            }
        }
        return null;

    }






}
