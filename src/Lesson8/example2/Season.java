package Lesson8.example2;

public enum Season {

    WINTER("Зима"),
    SPRING("Весна"),
    SUMMER("Лето"),
    AUTUMN("Осень");     //это свойства в виде объектов

    String seasonValue;

    Season (String seasonValue){    //входные параметры
        this.seasonValue = seasonValue;  //свойству присвоить значение входного параметра
    }

    public String getSeason(){
        return seasonValue;
    }



}
