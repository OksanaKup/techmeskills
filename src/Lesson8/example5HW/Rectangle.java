package Lesson8.example5HW;

public class Rectangle implements Shape{

    double weight;
    double height;

    public Rectangle (double weight, double height){
        this.weight = weight;
        this.height = height;
    }


    @Override
    public double getArea() {
        return weight * height;
    }
}
