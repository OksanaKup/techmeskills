package Lesson8.example5HW;

public class Circle implements Shape {

    double radius;    //S = pi * R2
    private double pi = 3.14;


    public Circle(double radius){
        this.radius = radius;
    }

    @Override
    public double getArea() {
        return radius * radius * pi;
    }
}
