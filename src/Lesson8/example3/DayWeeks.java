package Lesson8.example3;

public enum DayWeeks {

    MONDAY(1, "Понедельник"),
    TUESDAY(2, "Вторник"),
    WEDNESDAY(3, "Среда"),
    THURSDAY(4, "Четверг"),
    FRIDAY(5, "Пятница"),
    SATURDAY(6, "Суббота"),
    SUNDAY(7, "Воскресенье");

    int numberOfDayweek;
    String nameOfDayweek;

    DayWeeks(int numberOfDayweek, String nameOfDayweek){
        this.numberOfDayweek = numberOfDayweek;
        this.nameOfDayweek = nameOfDayweek;
    }

    public static String getValueOfDayWeekByNumber (int parametrFromScanner){
        DayWeeks[] dayWeeks = DayWeeks.values(); //у enum есть методы values, который возвращает объекты в массив
        for (DayWeeks dayWeek : dayWeeks){
            if (dayWeek.numberOfDayweek == parametrFromScanner){
                return dayWeek.nameOfDayweek;    //сначала возвращали просто объект, а теперь хотим вернуть Свойство объекта (ИМЯ)
            }
        }
        return null;
    }

}
