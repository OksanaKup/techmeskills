package lesson12.example3;

import java.util.*;

public class SetOfFruit {

    //Создать коллекцию HashSet с типом элементов String.
    //Добавить в неё 10 строк: арбуз, банан, вишня, груша, дыня, ежевика, жень-шень, земляника, ирис, картофель.
    //Вывести содержимое коллекции на экран, каждый элемент с новой строки.

    public static void main(String[] args) {

        Set<String> fruits = new LinkedHashSet<>();
        Scanner scan = new Scanner(System.in);
        for (int i = 0; i <= 5; i++) {
            String fruit = scan.next();       //теперь фрут положить во фрутс
            fruits.add(fruit);
        }
        for (String fruit : fruits) {
            System.out.println(fruit);
        }
//        Iterator value = fruits.iterator();
//        while (value.hasNext()){
//            System.out.println(value.next());
//        }
    }
}


