package lesson12.example2;

import java.util.*;

public class MapOfFruits {
    //Создать коллекцию HashMap<String, String>, занести туда 10 пар строк: арбуз – ягода,
    // банан – трава, вишня – ягода, груша – фрукт, дыня – овощ, ежевика – куст, жень-шень – корень, земляника – ягода, ирис – цветок, картофель – клубень.
    //Вывести содержимое коллекции на экран, каждый элемент с новой строки

    public static void main(String[] args) {
        Map<String, String> fruits = new HashMap<>();  //создание списка по порядку
        Scanner scan = new Scanner(System.in);
        for (int i = 0; i < 2; i++) {
            String name = scan.next();
            String type = scan.next();
            fruits.put(name, type);
        }
        for (Map.Entry <String, String> elem : fruits.entrySet()){
            System.out.println(elem);
//            System.out.println(elem.getKey());
//            System.out.println(elem.getValue());
        }

        System.out.println("Билдер");

        for (Map.Entry elem : fruits.entrySet()){
            StringBuilder builder = new StringBuilder();
            builder.append(elem.getKey()).append("-").append(elem.getValue());
            System.out.println(builder);
        }
    }
}







