package lesson12.example4;

import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.Set;

//с удалением элементов

public class SetFruitCollectionPro {
    public static void main(String[] args) {
        Set<String> fruits = new LinkedHashSet<>();  //создание списка по порядку
        Scanner scan = new Scanner(System.in);
        for (int i = 0; i <= 3; i++) {
            String fruit = scan.next();       //теперь фрут положить во фрутс
            fruits.add(fruit);
        }
        printAllFruits (fruits);
        System.out.println("Да - удалить элемент. Нет - завершить программу");
        String st = scan.next();

        while (!st.equals("Нет")) {
            if (st.equals("Да")) {
                System.out.println("Введите элемент для удаления");
                String temp = scan.next();
                fruits.remove(temp);
            }
            printAllFruits(fruits);
            System.out.println("Да - удалить элемент. Нет - не удалять");

            st = scan.next();
        }
    }
    static void printAllFruits(Set <String> fruits){
        for (String e : fruits) {
            System.out.println(e);
        }
    }
}
