package lesson2;

import java.util.Scanner;

public class HomeWorkTwo {

    public static void main(String[] args) {
        CountPos();
        CountPosNeg();
        MinArray();
        NumberProg();
    }

    private static void CountPos() {

        int[] num = new int[]{-8, 12, 5}; // даны три целых числа
        int countP = 0; // ввели понятие счетчика положительных чисел

        for (int i = 0; i < num.length; i++) {   //задаем условие прохождения цикла
            if (num[i] > 0)                     //задаем проверку каждого значения цикла
                countP++;                     //при соответствии значения условию увеличиваем счетчик положительных чисел на 1
        }
        System.out.println("Задание 1. Количество положительных чисел равно: " + countP); //выводим описание + полученное значение счетчика**/
    }

    private static void CountPosNeg() {
        int countPos = 0; // ввели понятие счетчика положительных чисел
        int countNeg = 0; // ввели понятие счетчика отрицательных чисел

        Scanner sc = new Scanner(System.in);
        System.out.println("Задание 2 ");
        System.out.println("Введите три целых числа ");
        int inputNum1 = sc.nextInt();
        int inputNum2 = sc.nextInt();
        int inputNum3 = sc.nextInt();

        if (inputNum1 > 0) {
            countPos++;
        } else countNeg++;
        if (inputNum2 > 0) {
            countPos++;
        } else countNeg++;
        if (inputNum3 > 0) {
            countPos++;
        } else countNeg++;

        System.out.println("Результат задания 2 ");
        System.out.println("Количество положительных чисел " + countPos);
        System.out.println("Количество отрицательных чисел " + countNeg);
    }

    private static void MinArray() {
        int[] arrKup = new int[]{46,10,0,15,-50};   // сам массив
        int minValue = arrKup[0];   // переменная, куда мы будем записывать минимальный элемент. Иницилизируем его первым эл-м массива (ячейка 0, поэтому arrKup[0], 46)
        for (int i = 1; i < arrKup.length; i++) {                   // i - это номер элемента массива
            if (arrKup[i] < minValue)   // за МинВалью мы берем 46
                minValue = arrKup[i];
        }
        System.out.println("Задание 3. Минимум:" + minValue);
    }

    private static void NumberProg() {
        System.out.println("Задание 4 ");
        for (int i = 1; i <= 10; i++) {
            switch (i) {
                case 1:
                    System.out.println(i + " программист");
                    continue;
                case 2:
                    System.out.println(i + " программиста");
                    continue;
                case 3:
                    System.out.println(i + " программиста");
                    continue;
                case 4:
                    System.out.println(i + " программиста");
                    continue;
                case 5:
                    System.out.println(i + " программистов");
                    continue;
                case 6:
                    System.out.println(i + " программистов");
                    continue;
                case 7:
                    System.out.println(i + " программистов");
                    continue;
                case 8:
                    System.out.println(i + " программистов");
                    continue;
                case 9:
                    System.out.println(i + " программистов");
                    continue;
                case 10:
                    System.out.println(i + " программистов");
                    break;
            }

        }
    }

}








