package lesson11.example2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Experimental {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        String a = scan.next();

        List<Integer> list = new ArrayList<>();

        while (!a.equals("stop")){
            //int valueFromStr = Integer.parseInt(a);

            try {
                list.add(Integer.valueOf(a));
            } catch (Exception ex) {
                System.out.println("Вы ввели некорректное значение. Попробуйте еще раз ");
            }
            a = scan.next();
        }

        int sumElem = 0;
        for (Integer elem : list){
            sumElem = sumElem + elem;
        }

        double result = sumElem/list.size();
        System.out.println(result);
    }
}
