package lesson11.example3;

import java.util.Objects;

public class Person {
    public static void main(String[] args) {

    }

    String name;

    Person (String name) {
        this.name = name;
    }

    public String getName (){
        return this.name;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return name.equals(person.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
