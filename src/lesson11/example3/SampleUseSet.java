package lesson11.example3;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.Set;

public class SampleUseSet {

    public static void main(String[] args) {

//        Set<String> collect = new HashSet<>();
//        collect.add("Petya");
//        collect.add("Petya");
//        collect.add("Petya2");
//
//        for (String elem : collect){
//            System.out.println(elem);
//        }

        Scanner sc = new Scanner(System.in);
        Set<Person> persons = new LinkedHashSet<>();

        String value = sc.next();
        while (!value.equals("stop")){
            //int valueFromStr = Integer.parseInt(a);
            Person person = new Person(value);
            persons.add(person);
            value = sc.next();
        }
        for (Person elem : persons){
            System.out.println(elem.getName());
        }


    }



}
