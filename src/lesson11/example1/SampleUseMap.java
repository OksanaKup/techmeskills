package lesson11.example1;

import java.util.HashMap;
import java.util.Map;

public class SampleUseMap {

    public static void main(String[] args) {

        Map<String, Integer> mapByInt = new HashMap<>();
        mapByInt.put("One" , 1);
        mapByInt.put("Two" , 2);
        mapByInt.put("Three" , 3);
        System.out.println(mapByInt.get("Three"));

        Map<Integer, String> mapByStr = new HashMap<>();
        mapByStr.put(1, "One");
        mapByStr.put(2, "Two");
        mapByStr.put(3, "Three");
        System.out.println(mapByStr.get("Three"));


    }


}
