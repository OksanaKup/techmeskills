package Lesson7.example4;

import java.util.Date;

public class AnimalService {

    public static void main(String[] args) {

        Cat cat = new Cat(1004, "Barsik", new Date(), "Blue");
        Dog dog = new Dog(1005, "Persik", new Date(), 56);
        Tiger tiger = new Tiger(1006, "Tigra", new Date(), "Red", 2);

        cat.displayInfo();
        dog.displayInfo();
        tiger.displayInfo();

    }

}
