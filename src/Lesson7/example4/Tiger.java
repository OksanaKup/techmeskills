package Lesson7.example4;

import java.util.Date;

public class Tiger extends Cat{

    int eatenExployees;

    public Tiger (int animalID, String name, Date date, String eyesColor, int eatenExployees){
        super(animalID, name, date, eyesColor);
        this.eatenExployees = eatenExployees;

    }
    @Override
    public void displayInfo() {
        System.out.println("ID: " + animalID + " " + "Name: " + name + " " + "Date " + date + " " + "Eyes color: " + eyesColor +
                " " + "Eaten exployees " + eatenExployees);
    }
}


