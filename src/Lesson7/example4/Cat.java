package Lesson7.example4;

import java.util.Date;

public class Cat extends Animal {


    public Cat(){};

    public String eyesColor;

    public Cat(int animalID, String name, Date date, String eyesColor){
        this.animalID = animalID;
        this.name = name;
        this.date = date;
        this.eyesColor = eyesColor;

    }


    @Override
    public void displayInfo() {
        System.out.println("ID: " + animalID + " " + "Name: " + name + " " + "Date " + date + " " + "Eyes color: " + eyesColor);
    }
}
