package Lesson7.example4;

import java.util.Date;

public class Dog extends Animal {

    double weight;

    public Dog (int animalID, String name, Date date, double weight){
        this.animalID = animalID;
        this.name = name;
        this.date = date;
        this.weight = weight;
    }

    @Override
    public void displayInfo() {
        System.out.println("ID: " + animalID + " " + "Name: " + name + " " + "Date " + date + " " + "Weight: " + weight);
    }
}
