package Lesson7.example3;

public class MessageService {

    public static void main(String[] args) {

        Telegram telegram = new Telegram();
//        telegram.getMessage();
//        telegram.sendMessage();

        WhatsApp whatsApp = new WhatsApp();
//        whatsApp.getMessage();
//        whatsApp.sendMessage();

        callMessenger(whatsApp);
        callMessenger(telegram);

    }

    private static void callMessenger(Messenger messenger){
        messenger.getMessage();
        messenger.sendMessage();


    }
}
