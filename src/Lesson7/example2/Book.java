package Lesson7.example2;

public class Book implements Printabe {

    private String name;
    private String author;
    private String year;

    //Чтобы нам эти значения как-то поместить в этот класс - надо их задать: можно это сделать через сеттеры (методы типа setName) или через конструктор


    public Book(String inputName, String inputAuthor, String inputYear){
        this.name = inputName;
        this.author = inputAuthor;
        this.year = inputYear;
    }

    @Override
    public void print() {
        System.out.println("Имя " + name + " " + "Author " + author + " " + "Year " + year);

    }
}
