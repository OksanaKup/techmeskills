package Lesson7.example1;

public class Employee extends Person {

    private String company;


    public Employee (String inputParamName, String inputParamSurname, String inputParamCompany){
        super(inputParamName, inputParamSurname);  //супер дано чтобы вызвать родительский объект, в данном случае Персон
        company = inputParamCompany;
    }

    public String getCompany(){
        return company;
    }

    @Override
    public void displayInfo(){
        System.out.println("Имя " + name + " " + "Фамилия " + surname + " " + "Компания " + company);
    }


    }
