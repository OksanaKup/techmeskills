package Lesson7.example1;

public class PersonService {
    public static void main(String[] args) {
        //exampleWorkWhenPersonNotAbstract();
        exampleTwoForAbstractClass();


    }

    private static void exampleTwoForAbstractClass(){
        Client client = new Client("Kirill", "Toros", "OTKRYTIE");
        client.displayInfo();

        Employee employee = new Employee("Aleksey", "Ivanov", "Fit System");
        employee.displayInfo();

    }


    /**объекты Персон можем создавать пока класс НЕ абстрактный**/
//    public static void exampleWorkWhenPersonNotAbstract(){
//
//        Person person = new Person();
//
//        System.out.println("Персона 1");
//        person.displayInfo();
////        System.out.print(person.getName() + " ");
////        System.out.println(person.getSurname());
//
//        Person person2 = new Person("Стас", "Васильев");
//        System.out.println("Персона 2");
//        System.out.print(person2.getName() + " ");
//        System.out.println(person2.getSurname());
//        Person person3 = new Person("Катя" , "Котова");
//
//        System.out.println("Персона 3");
//        System.out.print(person3.getName() + " ");
//        System.out.println(person3.getSurname());
//
//        Employee employee = new Employee("Оксана", "Евлантьева", "ТЕНЗОР");
//        System.out.println("Сотрудник 1");
//        //System.out.println(employee.displayInfo());
//        System.out.println(employee.getName());
//        System.out.println(employee.getSurname());
//        //System.out.println(employee.getc());
//
//        Employee employee2 = new Employee("Александр", "Осипов", "ЛАСТОЧКА КОМПАНИ");
//        System.out.println("Сотрудник 2");
//        employee2.displayInfo();
//
//        Employee employee3 = new Employee("Михаил", "Клещенков", "МАГНИТ");
//        System.out.println("Сотрудник 3");
//        employee3.displayInfo();
//        //System.out.println(employee3.getCompany());
//
//        Employee employee4 = new Employee("Kseniya", "Frolova", "Native Technologies");
//        employee4.displayInfo();
//
//    }




}
