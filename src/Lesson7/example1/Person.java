package Lesson7.example1;

public abstract class Person {
    protected String name;     //если приват, то не обратиться через объект (person.name). Для таких варианто делаются методы get
    protected String surname;

    Person(){}

    public Person (String inputParamName, String inputParamSurname){
        this.name = inputParamName;
        this.surname = inputParamSurname;

    }


    public String getName(){
//        if (name == null ){
//            return "Имя не задано "; }
//        else return name;
        return (name == null ? "Имя не задано " + "/" : name);
    }

    public String getSurname(){
        return surname == null ? "Фамилия не задана " : surname;
    }


    public abstract void displayInfo();





}
