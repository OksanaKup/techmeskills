package lesson6;

public class Cat {

    String name;
    int age;
    int gramm;

    Cat (String name, int age, int gramm){
        this.name = name;
        this.age = age;
        this.gramm = gramm;
    }

    public boolean cormezhka(){
        return gramm > 100;
    }



    @Override
    public String toString() {
        return "Имя кота " + name + " Возраст кота  " + age + " Количество корма в граммах " + gramm;
    }
}
