package lesson6;

import java.util.Random;

public class Otsenka {
    public static void main(String[] args) {

        String[] name = new String[]{"Саша", "Паша", "Даша", "Юля", "Наташа"};
        Student[] students = new Student[14];

        for (int i = 0; i < students.length; i++) {
            Student student = new Student(name[getRandom(4)],getRandom(3),getRandom(10));

//            if (student.getGrade() >= 9 ){
//                System.out.println(student.toString());
//            }

            students[i] = student;
        }

        System.out.println("Вывод отличников ");
        for (int i = 0; i < students.length; i++) {
            students[i].getOnlyGoodStudent();
        }


        System.out.println("Вывод всех ");
        for (int i = 0; i < students.length; i++){
            System.out.println (students[i].toString());
        }
    }

    private static int getRandom(int maxValue){
        Random random = new Random();
        return random.nextInt(maxValue);
    }

}
