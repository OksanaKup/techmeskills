package lesson6;
import java.util.Random;

public class Groupthree {
    public static void main(String[] args) {

        String[] name = new String[]{"Василий", "Стас", "Николай", "Петр", "Елена", "Екатерина"};
        Studenthw[] arrStud = new Studenthw[6];

        for (int i = 0; i < arrStud.length; i++){
            Studenthw studPers = new Studenthw(name[getRandom(4)], getRandom(4), getRandom(4));
            arrStud[i] = studPers;
        }

        System.out.println("Вывод всех ");
        for(int i = 0; i < arrStud.length; i++){
            System.out.println(arrStud[i].toString());
        }

        System.out.println("Вывод студентов группы номер 3 ");
        for(int i = 0; i < arrStud.length; i++){
            arrStud[i].getGroup();
        }
    }
    private static int getRandom(int maxValue){
        Random rand = new Random();
        return rand.nextInt(maxValue);
    }
}
