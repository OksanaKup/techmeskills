package lesson5.classWork;
//выводим данные по конструктору "Person.java"

public class Test2 {

    public static void main(String[] args) {

        Person deliver = new Person();                    //обращусь к классу Person, создам его экземпляр deliver (курьер)
        deliver.setHight(175);                            //ЭТО ЗАДАНИЕ ЗНАЧЕНИЯ через переменную. А ВЫВОД осуществляется методом getHigh, там return
        System.out.println(deliver.getHight());           //обращусь к методу из конструктора Person для вывода значения


        Person deliver2 = new Person(186);          //ЭТО ТОЖЕ ЗАДАНИЕ ЗНАЧЕНИЯ, а тут через конструктор
        System.out.println(deliver2.getHight());          //вывод значения методом, в котором прописали return (см. конструктор)

    }

}
