package lesson5.classWork;


public class Student {
    String name;
    int group;
    int grade;

    Student(){}

    Student(String name, int group, int grade){
        this.name = name;
        this.grade = grade;
        this.group = group;

    }

    public void setName(String name){
        this.name = name;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    @Override
    public String toString() {
        return "Имя: " + name + " Группа: " + group + " Оценка по диплому: " + grade;
    }                                                                                       //метод, который возвращает значение из класса Student

}