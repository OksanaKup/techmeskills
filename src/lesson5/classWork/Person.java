package lesson5.classWork;

//public class Person {
//
//    public int high;
//
//    Person (int high) {
//        this.high = high;
//    }
//    public int getHigh;
//}

    public class Person {
        public int hight;

        Person(){                               //пустой конструктор

        }
        Person(int hight){
            this.hight = hight;
        }                                       //тут он задал значения конструктором, а можно написать метод, который будет задавать значения

        public void setHight(int hight){
            this.hight = hight;                //МЕТОДОМ. this указывает на то, что эта переменная относится к полям данного класса
        }

        public int getHight(){
            return this.hight;
        }

    }