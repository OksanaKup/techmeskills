package lesson5.homeWork;

import java.util.Random;

public class UserCycle {
    public static void main(String[] args) {

        String[] name = new String[]{"Саша", "Паша", "Марина"};
        String[] login = new String[]{"sanya88", "pachan94", "marishka87"};
        User[] users = new User[3];


        for (int i = 0; i < users.length; i++){      //задаем значения массива
            User user = new User();  // для нового студента
            user.setName(name[getRandom(3)]);
            user.setLogin(login[getRandom(3)]);
            users[i] = user;
        }

        for (int i = 0; i < users.length; i++){
            System.out.println(users[i].toString());  //циклически выводим, одного раза недостаточно
        }
    }

    private static int getRandom (int maxValue){
        Random randUser = new Random();
        return randUser.nextInt(maxValue);

    }

}
