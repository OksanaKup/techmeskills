# Techmeskills

## Проект по обучению

Код к лекциям находится по [адресу](https://gitlab.com/AlexeyOs/techmeskills/)

Домашнее задание к уроку 8 находится по адресам:
1) [Вывод наименования месяца по его номеру](https://gitlab.com/OksanaKup/techmeskills/-/tree/main/src/Lesson8/example4HW)
2) [Расчет площадей фигур](https://gitlab.com/OksanaKup/techmeskills/-/tree/main/src/Lesson8/example5HW) 

Домашнее задание к уроку 9 находится по адресам:
1) [Ввод параметров фигур с консоли, расчет площадей фигур, периметра и длины окружности](https://gitlab.com/OksanaKup/techmeskills/-/tree/main/src/lesson9/example4HW)
Не получилось, не поняла как и куда добавить блоки кода, формирующие исключения для вводимых параметров. Попробовала на примере
окружности, через try catch в ShapeService
2) [Валидация на ввод цифр в фамилии](https://gitlab.com/OksanaKup/techmeskills/-/tree/main/src/lesson9/example5HW)

